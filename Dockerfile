FROM python:latest

LABEL maintainer="paloma.piot@udc.es"

RUN apt update && apt-get install -y gcc python3 musl 

ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt /requirements.txt

RUN pip install -U spacy
RUN python -m spacy download en_core_web_lg
RUN pip3 install -r requirements.txt 

RUN mkdir /profiler_api
WORKDIR /profiler_api
COPY ./ /profiler_api
EXPOSE 8000

RUN useradd -ms /bin/bash user
USER user

RUN python manage.py makemigrations && python manage.py migrate

CMD python manage.py runserver 0.0.0.0:8000