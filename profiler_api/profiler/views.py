import os
import pickle

from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_200_OK
)
from profiler_api.profiler.classifiers.gender.gender import preprocess
import lightgbm

GENDER_MODEL = pickle.load(open(os.path.join('profiler_api/profiler/classifiers/gender', 'gender77'), 'rb'))

# Create your views here.
@api_view(["POST"])
@permission_classes((AllowAny,))
def profile(request):
    comments = request.data.get("comments")
    experiment_id = request.data.get("experiment_id")
    comment_list = [x['comment'] for x in comments]
    data = preprocess(experiment_id, ' '.join(comment_list))
    print(data)
    # predict
    gender = GENDER_MODEL.predict(data.drop(['text', 'author_id'], axis=1))
    return Response({'result': 'OK', 'gender': gender}, status=HTTP_200_OK)